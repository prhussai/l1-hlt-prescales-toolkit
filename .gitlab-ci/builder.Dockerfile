# docker build -f builder.Dockerfile -t gitlab-registry.cern.ch/cms-cactus/experimental/l1-hlt-prescales-toolkit:$(date +%F) .

FROM cern/cc7-base:20191107

ENV GO_VERSION=1.13.6
ENV NODE_VERSION=12.14.1
ENV PATH="$PATH:/usr/local/go/bin:/usr/local/lib/nodejs/node-v${NODE_VERSION}-linux-x64/bin"

# yum packages
RUN yum install -y make xz-utils gcc git libxml2-devel gcc-c++ python3

# golang
RUN curl -o go.tar.gz https://dl.google.com/go/go${GO_VERSION}.linux-amd64.tar.gz && \
    tar -C /usr/local -xzf go.tar.gz && rm -f go.tar.gz

# nodejs
RUN curl -o node.tar.xz https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.xz && \
    mkdir -p /usr/local/lib/nodejs && tar -xJvf node.tar.xz -C /usr/local/lib/nodejs && rm node.tar.xz

# python stuff
RUN pip3 install pipenv pyinstaller