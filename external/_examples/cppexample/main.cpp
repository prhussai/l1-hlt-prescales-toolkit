#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <cerrno>

int fd_is_valid(int fd) {
    return fcntl(fd, F_GETFD) != -1 || errno != EBADF;
}

void writeFd3(std::string s) {
  size_t l = s.length();
  char p[l]; 
  int i; 
  for (i = 0; i < sizeof(p); i++) { 
      p[i] = s[i]; 
  } 
  write(3, p, l);
}

int main(int argc, char** argv) {
    if (fd_is_valid(3)) {
      std::cout << "running under pstools\n";
    } else {
      std::cout << "running standalone\n";
    }
    std::string name = "World";
    if (argc > 1) {
      name = argv[1];
    }
    // ssize_t written = write(3, "data", 4);
    std::string hello = "Hello, " + name + "!";
    std::cout << hello + "\n";
    writeFd3("{\"message\": \"" + hello + "\"}\n");
    return 0;
}