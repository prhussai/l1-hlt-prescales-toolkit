#!/usr/bin/env python3

import os
import sys
import time

try:
  os.stat(3)
  os.write(2, b'running under pstools\n')
  def write(s):
    return os.write(3, s)
except:
  os.write(2, b'running standalone\n')
  def write(s):
    return 0

name = "World"
if len(sys.argv) > 1:
  name = sys.argv[1]

hello = "Hello, " + name + "!"

os.write(1, bytes(hello + "\n", 'utf-8'))
write(bytes('{"hello": "' + hello + '"}\n', 'utf-8'))

# this can demonstrate streaming functionality in the CLI and API
os.write(1, b'sleeping for 1s\n')
time.sleep(1)

os.write(1, b'result2: value2\n')
write(b'{"result2": "value2"}\n')