package internal

import (
	"os"
	"syscall"
)

var fd3 *os.File

func init() {
	p := syscall.Flock_t{}
	err := syscall.FcntlFlock(3, syscall.F_GETFL, &p)
	if err == nil {
		fd3 = os.NewFile(3, "fd3")
	}
}

func writeFd3(msg []byte) {
	if fd3 != nil {
		fd3.Write(msg)
		return
	}
}
