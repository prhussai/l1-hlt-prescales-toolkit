package internal

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"time"

	"gopkg.in/yaml.v2"
)

// VetName is a name of the subfolder containing vetting code
type VetName string

// VetConfig represents a yaml config
type VetConfig struct {
	Run string
}

// VetType is a container for a VetName and its config
type VetType struct {
	name   VetName
	config VetConfig
}

func (n *VetName) fetchConfig() (*VetType, error) {
	configBytes, err := ioutil.ReadFile(path.Join("checks", string(*n), "vet.yaml"))
	if err != nil {
		return nil, err
	}
	c := VetConfig{}
	err = yaml.Unmarshal(configBytes, &c)
	if err != nil {
		return nil, err
	}
	return &VetType{*n, c}, nil
}

// VetTypes returns all valid vet types
func VetTypes() []VetType {
	result := make([]VetType, 0)
	files, err := ioutil.ReadDir("checks")
	if err != nil {
		log.WithError(err).Fatal("cannot read checks folder")
	}
	for _, f := range files {
		n := VetName(f.Name())
		t, err := n.fetchConfig()
		if err == nil {
			result = append(result, *t)
		}
	}
	return result
}

// ParseVetNames checks if all suplied names are valid vet names
func ParseVetNames(names ...string) ([]VetType, error) {
	all := VetTypes()
	parsed := make([]VetType, 0)
	for _, n := range names {
		var vetName VetName
		for _, v := range all {
			if string(v.name) == n {
				vetName = v.name
			}
		}
		if vetName == "" {
			log.WithField("name", n).WithField("vetnames", all).Error("invalid vet name")
			return nil, errors.New("invalid vet name")
		}
		vetType, err := vetName.fetchConfig()
		if err != nil {
			return nil, err
		}
		parsed = append(parsed, *vetType)
	}
	return parsed, nil
}

// VetAll runs the vetting logic of all valid vet names from VetNames()
func VetAll(xsdPath string, xmlPath string) error {
	all := VetTypes()
	return Vet(xsdPath, xmlPath, all...)
}

// Vet performs the vetting logic for the specified vet names
func Vet(xsdPath string, xmlPath string, todo ...VetType) error {
	for _, v := range todo {
		log.WithField("vet name", v).Info("performing vet")

		cmd := exec.Command(string(v.config.Run), xsdPath, xmlPath)
		cmd.Dir = path.Join("checks", string(v.name))
		cmd.Stdout, cmd.Stderr = os.Stdout, os.Stderr

		fd3 := make(chan string)
		doneReading := make(chan error)
		fd3cancel, err := attachFd3(cmd, fd3, doneReading)
		if err != nil {
			return err
		}

		go func() {
			for line := range fd3 {
				var payload interface{}
				err = json.Unmarshal([]byte(line), &payload)
				if err != nil {
					payload = line
				}
				payload = fd3Entry{v.name, payload}
				entry, err := json.Marshal(payload)
				if err != nil {
					log.WithError(err).WithField("entry", payload).Error("cannot marshal fd3 json")
				} else {
					writeFd3(entry)
				}
			}
		}()

		err = cmd.Start()

		timeout := time.After(10 * time.Second)
		doneScripting := make(chan error)
		go func() {
			doneScripting <- cmd.Wait()
			close(doneScripting)
		}()

		select {
		case <-timeout:
			cmd.Process.Kill()
			fd3cancel.Close()
			return errors.New("script timed out")
		case err := <-doneScripting:
			fd3cancel.Close()
			if err != nil {
				return err
			}
		}
		log.WithField("vet name", v).Info("vet done")
	}
	return nil
}
