package internal

import (
	"io"
	"os"
	"os/exec"
)

func attachFd3(cmd *exec.Cmd, lines chan string, done chan error) (io.Closer, error) {
	fd3r, fd3w, err := os.Pipe()
	if err != nil {
		return nil, err
	}
	cmd.ExtraFiles = []*os.File{fd3w}

	go readerToChannel(fd3r, lines, done)

	return fd3w, nil
}
