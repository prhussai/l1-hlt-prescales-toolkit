package internal

import (
	"bufio"
	"io"
)

func readerToChannel(r io.Reader, lines chan string, done chan error) {
	s := bufio.NewScanner(r)
	s.Split(bufio.ScanLines)
	for s.Scan() {
		lines <- s.Text()
	}
	close(lines)
	done <- nil
}
