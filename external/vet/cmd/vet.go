package main

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"pstools-vet/internal"

	"github.com/spf13/cobra"
)

var rawXML *bool
var rawXSD *bool

var rootCommand = &cobra.Command{
	Use:   "vet [XSDPath] [XMLPath|XML] [checklist]",
	Short: "Perform the given vets by name on the given XML",
	Args:  cobra.MinimumNArgs(3),
	Run: func(cmd *cobra.Command, args []string) {
		log.Info("initializing vetting")

		// in the end, we need an XSD & XML file path
		xsdPath := args[0]
		var err error
		if *rawXSD {
			xsdPath = makeFile(xsdPath)
			defer os.Remove(xsdPath)
		} else {
			xsdPath, err = filepath.Abs(xsdPath)
			if err != nil {
				log.WithError(err).WithField("path", xsdPath).Fatal("cannot get absolute path")
			}
		}
		xmlPath := args[1]
		if *rawXML {
			xmlPath = makeFile(xmlPath)
			defer os.Remove(xmlPath)
		} else {
			xmlPath, err = filepath.Abs(xmlPath)
			if err != nil {
				log.WithError(err).WithField("path", xmlPath).Fatal("cannot get absolute path")
			}
		}

		var todo []internal.VetType
		if args[2] == "all" {
			todo = internal.VetTypes()
		} else {
			var err error
			todo, err = internal.ParseVetNames(strings.Split(args[2], ",")...)
			if err != nil {
				log.WithError(err).Fatal("vet name parsing failed")
			}
		}
		log.WithField("vets", todo).Info("starting vetting")

		err = internal.Vet(xsdPath, xmlPath, todo...)
		if err != nil {
			log.WithError(err).Fatal("vetting failed")
		}
		log.Info("vetting succeeded")
	},
}

func makeFile(raw string) string {
	var file *os.File
	file, err := ioutil.TempFile("", "temp-pstools-")
	if err != nil {
		log.WithError(err).Fatal("cannot write temporary XSD file")
	}
	fileName := file.Name()
	file.WriteString(raw)
	file.Close()
	return fileName
}

func init() {
	rawXSD = rootCommand.PersistentFlags().Bool("rawXSD", false, "XSD argument is XML content instead of a file path")
	rawXML = rootCommand.PersistentFlags().Bool("rawXML", false, "XML argument is XML content instead of a file path")
}

func main() {
	err := rootCommand.Execute()
	if err != nil {
		log.WithError(err).Fatal("unable to start CLI")
	}
}
