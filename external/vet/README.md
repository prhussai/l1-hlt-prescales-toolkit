# Pstools vetting

This is a special-case external integration.

Every sub-folder is a vetting check to be performed.

It expects a Makefile, like other 'external' subprojects, and a vet.yaml specifying how to run your vetting code.

Your vetting code will receive the following arguments:
 - an absolute path to the prescale XSD
 - an absolute path to the prescale XML