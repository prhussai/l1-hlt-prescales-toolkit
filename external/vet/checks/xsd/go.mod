module pstools-vet-xsd

go 1.13

require (
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/terminalstatic/go-xsd-validate v0.1.2
	golang.org/x/sys v0.0.0-20200212091648-12a6c2dcc1e4 // indirect
)
