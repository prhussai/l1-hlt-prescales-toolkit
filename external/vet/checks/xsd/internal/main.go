package internal

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"

	xsdvalidate "github.com/terminalstatic/go-xsd-validate"
)

func Validate() {
	xsdPath, xml, err := parseArgs()
	if err != nil {
		log.WithError(err).Fatal("cannot parse arguments")
	}

	xsdvalidate.Init()
	defer xsdvalidate.Cleanup()
	xsdHandler, err := xsdvalidate.NewXsdHandlerUrl(xsdPath, xsdvalidate.ParsErrDefault)
	if err != nil {
		log.WithError(err).Fatal("cannot read XSD")
	}
	defer xsdHandler.Free()

	err = xsdHandler.ValidateMem(xml, xsdvalidate.ParsErrVerbose)
	if err != nil {
		reportError(err)
		log.WithError(err).Fatal("XSD validation failed")
	}
	log.Info("XSD validation passed")
}

func parseArgs() (xsdPath string, xml []byte, err error) {
	if len(os.Args) < 3 {
		log.Fatal("not all required arguments given")
	}
	xsdPath = os.Args[1]
	xmlPath := os.Args[2]
	log.WithField("xsd", xsdPath).WithField("xml", xmlPath).Info("starting xsd validation")

	xml, err = ioutil.ReadFile(xmlPath)
	if err != nil {
		log.WithError(err).Fatal("cannot read xml")
	}
	return
}

func reportError(err error) {
	if validationErr, ok := err.(xsdvalidate.ValidationError); ok {
		for _, err := range validationErr.Errors {
			writeAnnotation(err)
		}
	} else if parserError, ok := err.(xsdvalidate.XmlParserError); ok {
		structError := xsdvalidate.StructError{
			Code:     0,
			Level:    3, // http://www.xmlsoft.org/html/libxml-xmlerror.html#xmlErrorLevel
			Line:     0,
			NodeName: "",
			Message:  parserError.Error(),
		}
		r, e := regexp.Compile("line ([0-9]+)")
		if e != nil {
			fmt.Fprintln(os.Stderr, "cannot compile regex", e)
			os.Exit(1)
		}
		matches := r.FindStringSubmatch(parserError.Error())
		if len(matches) > 1 {
			i, _ := strconv.ParseInt(matches[1], 0, 32)
			structError.Line = int(i)
		}
		writeAnnotation(structError)
	} else if err != nil {
		structError := xsdvalidate.StructError{
			Code:     0,
			Level:    3, // http://www.xmlsoft.org/html/libxml-xmlerror.html#xmlErrorLevel
			Line:     0,
			NodeName: "",
			Message:  err.Error(),
		}
		writeAnnotation(structError)
	}
}

func writeAnnotation(structError xsdvalidate.StructError) {
	bytes, err := json.Marshal(structError)
	if err != nil {
		writeFd3([]byte(structError.Message + "\n"))
	} else {
		writeFd3(bytes)
		writeFd3([]byte("\n"))
	}
	fmt.Fprintln(os.Stderr, structError.Message)
}
