package main

import (
	"pstools-vet-xsd/internal"
)

func main() {
	internal.Validate()
}
