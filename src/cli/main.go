package cli

import (
	"os"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

var rootCmd = &cobra.Command{
	Use:   "L1+HLT PS Tools",
	Short: "Level-1 & HLT Prescales toolkit",
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config.file", "", "config file")
}

// InitCli adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func InitCli() {
	err := rootCmd.Execute()
	if err != nil {
		log.WithError(err).Fatal("cannot start CLI")
	}
}

func initConfig() {
	configFolder := os.Getenv("XDG_CONFIG_HOME")
	if configFolder == "" {
		home := os.Getenv("HOME")
		if home != "" && !strings.HasSuffix(home, "/") {
			home = home + "/"
		}
		configFolder = home + ".config/pstools/"
	}

	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		viper.AddConfigPath(configFolder)
		// read https://refspecs.linuxfoundation.org/FHS_3.0/fhs-3.0.pdf
		// page 9 specifies why to prefer /etc/opt
		viper.AddConfigPath("/etc/opt/cactus/pstools/config/")
		viper.AddConfigPath(".")
		viper.SetConfigName("config")
	}

	// read in env variables, env variables override config file
	// example: 'loglevel: info' in config.yaml will be overriden by command
	//          'L1HLTPS_LOGLEVEL=ERROR go run main.go'
	viper.SetEnvPrefix("PSTOOLS")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	err := viper.ReadInConfig()
	if err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.WithError(err).WithField("file", cfgFile).Fatal("specified config file not found")
		} else {
			log.WithError(err).WithField("file", viper.ConfigFileUsed()).Fatal("an error occurred while reading config file")
		}
	}
	log.WithField("file", viper.ConfigFileUsed()).Info("config file found")
}
