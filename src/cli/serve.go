package cli

import (
	"l1-hlt-prescales-toolkit/src/web/api"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var serveCmd = &cobra.Command{
	Use:     "serve",
	Aliases: []string{"ui", "gui", "server"},
	Short:   "Start a webserver with the prescale editor",
	Run: func(cmd *cobra.Command, args []string) {
		api.StartAndListen(*port)
	},
}

var port *string

func init() {
	rootCmd.AddCommand(serveCmd)
	port = serveCmd.Flags().StringP("port", "p", "", "set server port")
	viper.BindPFlag("api.port", serveCmd.Flags().Lookup("port"))
}
