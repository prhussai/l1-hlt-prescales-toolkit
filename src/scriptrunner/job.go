package scriptrunner

import (
	"time"
)

// Job is an object to initialize a command to run with RunAsync()
type Job struct {
	Dir        string
	ScriptPath string
	Arg        []string
	Timeout    time.Duration
}

var defaultTimeout = 20 * time.Second

// NewJob creates a new Job object
func NewJob(dir string, scriptPath string, arg []string, timeout *time.Duration) *Job {
	var t time.Duration
	if timeout == nil {
		t = defaultTimeout
	} else {
		t = *timeout
	}
	return &Job{dir, scriptPath, arg, t}
}
