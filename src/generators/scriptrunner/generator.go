//go:generate go run generator.go
// +build ignore

package main

import (
	scriptrunner "l1-hlt-prescales-toolkit/src/generators/scriptrunner"
	"l1-hlt-prescales-toolkit/src/generators/util"
	"os"
)

func main() {
	if len(os.Args) > 1 && os.Args[1] != "" {
		scriptrunner.GenerateScriptrunner(util.ExternalPackage(os.Args[1]))
	} else {
		scriptrunner.GenerateAll()
	}
}
