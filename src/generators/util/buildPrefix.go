package util

import "strings"

// BuildPrefix takes the run string from config, and returns the last space-splitted slice
func BuildPrefix(run string) string {
	pieces := strings.Split(run, " ")
	return pieces[len(pieces)-1]
}
