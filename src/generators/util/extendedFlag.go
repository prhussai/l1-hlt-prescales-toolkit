package util

import (
	"fmt"
	"reflect"
)

// ExtendedFlag is an ExternalConfigFlag with typing information
type ExtendedFlag struct {
	ExternalConfigFlag
	GoType        string
	ZeroValue     string
	QuotedDefault string
}

// ExtendFlags returns the results of Extend() on all given flags
func ExtendFlags(flags []ExternalConfigFlag) []ExtendedFlag {
	e := make([]ExtendedFlag, len(flags))
	for i, flag := range flags {
		e[i] = flag.Extend()
	}
	return e
}

// Extend turns the ExternalConfigFlag into an ExtendedFlag
func (flag *ExternalConfigFlag) Extend() ExtendedFlag {
	var t reflect.Type
	// if flag type is not explicitly given, infer it
	if flag.ValueType == "" {
		// no default value -> assume string
		if flag.DefaultValue == nil {
			t = reflect.TypeOf("")
		} else {
			t = reflect.TypeOf(flag.DefaultValue)
		}

		// if flag type is explicitly given, take that one
	} else {
		switch flag.ValueType {
		case "string":
			t = reflect.TypeOf("")
		case "int":
			t = reflect.TypeOf(int(0))
		case "int8":
			t = reflect.TypeOf(int8(0))
		case "int16":
			t = reflect.TypeOf(int16(0))
		case "int32":
			t = reflect.TypeOf(int32(0))
		case "int64":
			t = reflect.TypeOf(int64(0))
		case "bool":
			t = reflect.TypeOf(bool(false))
		default:
			panic("type " + flag.ValueType + " not supported")
		}
	}

	goType := t.Name()
	zeroValue := fmt.Sprint(reflect.Zero(t))
	quotedDefault := ""
	if goType == "string" {
		zeroValue = "\"\""
		if flag.DefaultValue == nil {
			quotedDefault = "\"\""
		} else {
			quotedDefault = fmt.Sprintf("\"%v\"", flag.DefaultValue)
		}
	} else {
		if flag.DefaultValue != nil {
			quotedDefault = fmt.Sprintf("%v", flag.DefaultValue)
		}
	}
	return ExtendedFlag{*flag, goType, zeroValue, quotedDefault}
}
