//go:generate go run generator.go
// +build ignore

package main

import (
	cligenerator "l1-hlt-prescales-toolkit/src/generators/cli"
	"l1-hlt-prescales-toolkit/src/generators/util"
	"os"
)

func main() {
	if len(os.Args) > 1 && os.Args[1] != "" {
		cligenerator.GenerateCLICommand(util.ExternalPackage(os.Args[1]))
	} else {
		cligenerator.GenerateAll()
	}
}
