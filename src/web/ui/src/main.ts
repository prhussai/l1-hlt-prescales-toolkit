import Vue from "vue";
import App from "./App.vue";
import { stores } from "./store";
import "./style";

Vue.config.productionTip = false;

new Vue({
  store: stores.root,
  render: h => h(App),
}).$mount("#app");
