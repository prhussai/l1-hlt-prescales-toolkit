import Vue from "vue";
import Vuex from "vuex";

import createMutationsSharer from "vuex-shared-mutations";
import { getModule } from "vuex-module-decorators";

import { XMLModule } from "./modules/xml";

Vue.use(Vuex);

const rootStore = new Vuex.Store({
  plugins: [createMutationsSharer({ predicate: () => true })],
  state: {
    message: "hello",
  },
  mutations: {
    setMessage(state, message) {
      state.message = message;
    },
    brainTransfer(state, newstate) {
      Object.assign(state, newstate);
    },
  },
  actions: {},
  modules: {
    xml: XMLModule,
  },
});

export const stores = {
  root: rootStore,
  xml: <XMLModule>getModule(XMLModule, rootStore),
};

stores.xml.getRemoteXML();
