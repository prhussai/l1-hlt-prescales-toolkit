import { Layout } from "./layout";
import { workspaces } from "./workspaces";

export let layout: Layout;

export const init = (mount: HTMLElement) => {
  if (layout) {
    // eslint-disable-next-line no-console
    console.error("cannot mount main layout twice");
    return;
  }
  layout = new Layout("pstools", mount, workspaces["layout A"]);
};
