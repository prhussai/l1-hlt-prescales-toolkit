import { panels, panelTitle, panelState } from "../../panels";
import { ComponentConfig } from "golden-layout";

export function makePanelConfig(title: panelTitle): ComponentConfig {
  return {
    title,
    type: "component",
    componentName: "vueComponent",
    componentState: <panelState>{ title },
  };
}

export function getPanelTitle(title: string): panelTitle | void {
  if (!panels[title as panelTitle]) {
    // eslint-disable-next-line no-console
    console.error(
      "no panel exists with this name",
      title,
      "valid options are",
      Object.keys(panels)
    );
    return;
  }
  return title as panelTitle;
}
