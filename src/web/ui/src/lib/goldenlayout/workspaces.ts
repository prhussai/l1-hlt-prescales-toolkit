import { ItemConfigType } from "golden-layout";
import { makePanelConfig as panel } from "./makePanelConfig";

export type workspaceName = "layout A" | "layout B";

export const workspaces: { [key in workspaceName]: ItemConfigType[] } = {
  "layout A": [
    {
      type: "row",
      content: [
        panel("I am"),
        {
          type: "column",
          content: [panel("Table"), panel("Editor")],
        },
      ],
    },
  ],

  "layout B": [
    {
      type: "row",
      content: [panel("Editor"), panel("I am"), panel("Editor")],
    },
  ],
};

export const defaultWorkspace = workspaces["layout A"];
