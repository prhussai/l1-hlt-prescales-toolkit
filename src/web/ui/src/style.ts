import "reset-css/sass/_reset.scss";
import "golden-layout/src/css/goldenlayout-base.css";
//import "golden-layout/src/css/goldenlayout-light-theme.css";
//import "golden-layout/src/css/goldenlayout-dark-theme.css";

import "ag-grid-community/src/styles/ag-grid.scss";
import "ag-grid-community/src/styles/ag-theme-balham-dark/sass/ag-theme-balham-dark.scss";

import "./styles/goldenlayout-vue.scss";
