import { VueConstructor } from "vue/types/umd";

//////////
// Add panel names here
//////////
type PanelName = "I am" | "Editor" | "Table" | "Manual Editor";
const items: PanelsConf = {
  "I am": "IAm",
  Editor: "Edit",
  Table: "Table",
  "Manual Editor": "Code",
};

//////////
// Creates lazy loaders from previous panel configs
//////////
type PanelsConf = {
  [t in PanelName]: string;
};
type Loader = () => Promise<VueConstructor<Vue>>;
type PanelsLoader = {
  [t in PanelName]: Loader;
};
export const panels = panelsList(items);

function panelsList(items: PanelsConf): PanelsLoader {
  // string instead of PanelName so the compiler doesn't complain before we
  // get a chance to initialize
  const panels: { [name: string]: Loader } = {};
  Object.keys(items).forEach(
    title =>
      (panels[title as PanelName] = () =>
        import("./" + items[title as PanelName] + ".vue").then(s => s.default))
  );
  return panels as PanelsLoader;
}

//////////
// Types
//////////
export type panelTitle = keyof typeof panels;
export interface panelState {
  title: panelTitle;
}
