package apiv1

import (
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

var v1router = makeRouter()

// Register adds the v1router to the given router
func Register(router *chi.Mux) {
	router.Mount("/api/v1", v1router)
}

func makeRouter() *chi.Mux {
	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(panicHandler)
	// Set a timeout value on the request context (ctx), that will signal
	// through ctx.Done() that the request has timed out and further
	// processing should be stopped
	r.Use(middleware.Timeout(5 * time.Minute))

	r.NotFound(statusReturner(http.StatusNotFound))
	r.MethodNotAllowed(statusReturner(http.StatusMethodNotAllowed))
	return r
}

func statusReturner(code int) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		Error(code).Send(w, r)
	}
}
