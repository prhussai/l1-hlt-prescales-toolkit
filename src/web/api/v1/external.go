package apiv1

import (
	"github.com/go-chi/chi"
)

var externalRouter = chi.NewRouter()

type argName string
type argConf struct {
	Name         string
	ArgType      string
	DefaultValue interface{} `json:",omitempty"`
}

type externalConfig struct {
	Command     string
	Deprecated  string              `json:",omitempty"`
	Description string              `json:",omitempty"`
	Args        map[argName]argConf `json:"arguments"`
}

func init() {
	v1router.Mount("/external", externalRouter)
}
